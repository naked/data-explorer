#! /usr/bin/python2

from math import floor
from struct import unpack

import numpy

from scipy import signal
from scipy import fft
from scipy import fftpack

from matplotlib.figure import Figure
from matplotlib.backends.backend_gtk3agg import FigureCanvasGTK3Agg as FigureCanvas
from matplotlib.backends.backend_gtk3 import NavigationToolbar2GTK3 as NavigationToolbar

class Plot(object):
    def __init__(self):
        """
            Object to deal with plotting
        """
        self.figure = Figure()

        # Constant lookup tables
        self.offsets = {
                "X": 0,
                "Y": 1,
                "Z": 2
                }

        self.colors = {
                "X": "r-",
                "Y": "g-",
                "Z": "b-"
                }

        self.vals = {}

    def getcanvas(self):
        self.canvas = FigureCanvas(self.figure)
        self.canvas.show()
        return self.canvas

    def unpack_data(self, axis):
        data = self.data
        unpacked = [ unpack('H', "".join([data[i], data[i+1]]))[0] for i in range(0, len(data), 2) ]
        self.vals[axis] = numpy.array([ str(unpacked[i]) for i in range(self.offsets[axis], len(unpacked), 3) ], dtype=numpy.float64)


        if self.smooth:
            w = numpy.ones(self.smooth)/self.smooth
            self.signal = signal.lfilter(w,1,self.vals[axis])[100:]      # clip first samples
            self.vals[axis] = self.signal
        else:
            self.signal = self.vals[axis]

        return

    def periodogram_update(self, data, smooth=False, axis_list=["X"]):
        self.figure.clear()
        self.data = data
        self.smooth = smooth
        plot_id = 1

        #self.vals[axis] *= window

        for axis in axis_list:
            self.unpack_data(axis)
            #window = signal.hamming(len(self.vals[axis]))

            freqs, p_gram = signal.periodogram(abs(self.signal), self.samplerate, 'hanning')
            #FFT = fft(abs(self.signal), 414)
            #freqs = fftpack.fftfreq(FFT.size, float(1./207))

            a = self.figure.add_subplot(1,1,plot_id)
            a.plot(freqs, p_gram, self.colors[axis])
            a.set_xlim(0, 10)
            #a.set_ylim(0, 100)
            a.relim()

            plot_id += 1

        self.canvas.draw()
        return

    def fft_update(self, data, smooth=False, axis_list=["X"]):
        self.figure.clear()
        self.data = data
        self.smooth = smooth
        plot_id = 1

        for axis in axis_list:
            self.unpack_data(axis)
            FFT = fft(abs(self.signal), 414)

            freqs = fftpack.fftfreq(FFT.size, float(1./207))

            a = self.figure.add_subplot(1,1,plot_id)
            a.plot(freqs, FFT, self.colors[axis])
            a.set_xlim(0, 20)
            a.set_ylim(0, 100)

            plot_id += 1

        self.canvas.draw()
        return

    def normal_update(self, data, smooth=False, axis_list=["X"]):

        self.figure.clear()
        self.data = data
        self.smooth = smooth

        for axis in axis_list:
            #current_axis = axis + "_val"
            self.unpack_data(axis)



            #if len(s) > 5000:
            #    TODO: Make a decimate slider?
            #    s = signal.decimate(s, 10)

            x_axis = range(0, len(self.signal));

            a = self.figure.add_subplot(1,1,1)
            a.set_ylim(300, 800)

            line, = a.plot(x_axis, self.signal, self.colors[axis])
            line.set_xdata(x_axis)
            line.set_ydata(self.signal)

        a.relim()
        a.autoscale_view(True, True, False)
        self.canvas.draw()

if __name__ == "__main__":
    plot(read_range(0,2))
