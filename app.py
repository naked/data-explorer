#! /usr/bin/python2

# March 16 2014
# Christopher Toledo

# Custom modules
from plot import Plot as plot

# Built-in and vendor modules
from os import path
from gi.repository import Gtk

import numpy

from scipy import signal

# constants
block_size = 512

class MainApp(object):


    def __init__(self):
        print "Initializing App"
        
        # Load default file
        self.load_file("data.bin")

        # Create our initial plot object
        self.plotter = plot()
        self.plotter.samplerate = 38  #default samplerate

        self.builder = Gtk.Builder()
        self.builder.add_from_file("data-explorer.glade")
        self.samplerate_builder = Gtk.Builder()

        self.samplerate_builder.add_from_file("samplerate-dialog.glade")

        dialog_signals = {
                "samplerate_ok": self.samplerate_ok,
                "samplerate_cancel": self.samplerate_cancel,
                "delete-event": None
                }

        self.samplerate_builder.connect_signals(dialog_signals)
        self.samplerate_dialog = self.samplerate_builder.get_object("samplerate_dialog")

        self.zoom = 1
        self.smooth = False
        self.fft_active = False

        self.zoom_scale = self.builder.get_object("zoom_scale")
        self.position_scale = self.builder.get_object("position_scale")
        self.smooth_scale = self.builder.get_object("smooth_scale")

        self.zoom_scale.set_range(2, 30)
        self.position_scale.set_range(0, (self.num_blocks/self.zoom) - 1)

        self.smooth_scale.set_range(1, 100)
        self.smooth_scale.set_sensitive(False)

        self.x_checkmenu = self.builder.get_object("x_checkmenu")
        self.x_checkmenu.set_active(True)

        plot_radio = self.builder.get_object("normal_plotview") # set default plotview
        plot_radio.set_active(True)

        self.statusbar = self.builder.get_object("statusbar")

        signals = {
                "on_window_destroy": self.quit,
                "zoom_scale_value_changed": self.update_zoom,
                "position_scale_value_changed": self.update_plot,
                "smooth_checkbutton_toggled": self.toggle_smooth,
                "smooth_scale_value_changed": self.update_smooth_value,
                "x_checkmenu_toggled": self.toggle_view_axis,
                "y_checkmenu_toggled": self.toggle_view_axis,
                "z_checkmenu_toggled": self.toggle_view_axis,
                "set_samplerate_activate": self.samplerate_activate,
                "plotview_radio_toggled": self.normal_view_toggle
                }

        self.builder.connect_signals(signals)

        plot_window = self.builder.get_object("plot_holder")
        self.canvas = self.plotter.getcanvas()
        plot_window.add_with_viewport(self.canvas)

        self.toggle_view_axis()     # Update our object with axis view data

    def samplerate_ok(self, *args):
        self.samplerate_spinner = self.samplerate_builder.get_object("samplerate_spin")
        self.plotter.samplerate = self.samplerate_spinner.get_value_as_int()
        self.update_plot()
        self.samplerate_dialog.hide()

    def samplerate_cancel(self, *args):
        self.samplerate_dialog.hide()

    def samplerate_activate(self, *args):
        self.samplerate_dialog.show_all()
        result = self.samplerate_dialog.run()
        self.samplerate_dialog.hide()
        #self.samplerate_dialog.destroy()

    def normal_view_toggle(self, *args):
        self.fft_active = not args[0].get_active()
        #print "FFT plot?:" + str(self.fft_active)
        self.update_plot()

    def update_zoom(self, *args):
        self.zoom = int(self.zoom_scale.get_value())
        self.position_scale.set_range(0, (self.num_blocks/self.zoom))
        self.update_plot()

    def load_file(self, data_file="data.bin"):
        self.data_file = data_file
        self.file_size = path.getsize(data_file)         # File size in bytes
        self.num_blocks = self.file_size/block_size      # Number of blocks in our data file

    def read_range(self, start, end):
        self.chunk_size = block_size * self.zoom         # zoom is # of blocks in a chunk

        data = ""

        file = open(self.data_file, "rb")

        for sector in range(start * self.chunk_size, end * self.chunk_size, 512):
            file.seek(sector)
            data += file.read(510)

        file.close()

        return data

    def toggle_view_axis(self, *args):
        menu4 = self.builder.get_object("menu4")
        active_axis = [] 

        for item in menu4.get_children():
            if item.get_active():
                active_axis.append(item.get_label())

        self.axis_list = active_axis
        self.update_plot()

        return active_axis

    def toggle_smooth(self, *args):
        smooth_checkbutton = self.builder.get_object("smooth_checkbutton")
        smooth_on = smooth_checkbutton.get_active()

        if smooth_on:
            self.smooth = self.smooth_scale.get_value()
            self.smooth_scale.set_sensitive(True)
        else:
            self.smooth = False
            self.smooth_scale.set_sensitive(False)

        self.update_plot()

    def update_smooth_value(self, *args):
        self.smooth = self.smooth_scale.get_value()
        self.update_plot()

    def update_plot(self, *args):

        if len(self.axis_list) == 0:
            self.update_status_bar("No axis selected!")
            return

        self.position = int(self.position_scale.get_value())
        self.zoom = int(self.zoom_scale.get_value())
        self.data = self.read_range(self.position, self.position + self.zoom)

        if self.fft_active == True:
            #self.plotter.fft_update(self.data, self.smooth, self.axis_list)
            self.plotter.periodogram_update(self.data, self.smooth, self.axis_list)
        else:
            self.plotter.normal_update(self.data, self.smooth, self.axis_list)

    def update_status_bar(self, status, *args):
        self.statusbar.push(1, status)

    def quit(self, *args):
        print "Quitting App"
        Gtk.main_quit()


if __name__ == "__main__":
    app = MainApp()
    Gtk.main()
